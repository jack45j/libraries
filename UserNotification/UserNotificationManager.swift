//
//  UNUserNotificationManager.swift
//  CurDr_TW_Doctor
//
//  Created by 林翌埕 on 2018/10/1.
//  Copyright © 2018 Benson Lin. All rights reserved.
//

import UserNotifications
import UIKit

protocol UserNotificationManagerDelegate{
    func getDeviceToken(token pushNotificationTokenString: String)
}

class UserNotificationManager {
    
    /// set to true/false to enable/disable UNM log.
    internal lazy var isDebugEnable = true
    
    /// UNM singleton.
    internal static let shared = UserNotificationManager()
    
    /// UNM protocol.
    var delegate: UserNotificationManagerDelegate?
    
    /// UNM DataModel class
    /// see UserNotificationDataModelClass.swift for more detail.
    var pushNotificationData: PushNotificationDataModelClass?
    
    /// UNM denied alertController configuration.
    var deniedAlertTitle: String? = "您尚未開啟推播功能"
    var deniedAlertMessage: String? = "為了您良好的使用體驗，請開啟您的推播通知功能"
    var deniedAlertStyle: UIAlertController.Style = .alert
    var deniedAlertCancelButtonString: String? = "取消"
    var deniedAlertSettingsButtonString: String? = "設定"
    
    internal func requestNotification(options: UNAuthorizationOptions = [.alert, .sound, .badge]) {
        UNUserNotificationCenter.current().requestAuthorization(options: options) { (permissionGranted, error) in
            
            if !permissionGranted {
                self.printUserNotificationLog("Permission denied.")
                let requestNotificationPermissionAlertController = UIAlertController(title: self.deniedAlertTitle!,
                                                                                     message: self.deniedAlertMessage!,
                                                                                     preferredStyle: self.deniedAlertStyle)
                let cancelAlertAction = UIAlertAction(title: self.deniedAlertCancelButtonString, style: .cancel, handler: nil)
                let settingAlertAction = UIAlertAction(title: self.deniedAlertSettingsButtonString, style: .destructive, handler: { (_) in
                    if !UIApplication.shared.isRegisteredForRemoteNotifications {
                        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                    }
                })
                requestNotificationPermissionAlertController.addAction(settingAlertAction)
                requestNotificationPermissionAlertController.addAction(cancelAlertAction)
                UIApplication.shared.keyWindow?.rootViewController?.present(requestNotificationPermissionAlertController, animated: true, completion: nil)
            } else {
                self.printUserNotificationLog("Successfully granted remote notification permission.")
            }
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    fileprivate func didReceiveRemoteNotification(userInfo: [AnyHashable : Any]) {
        self.printUserNotificationLog("Received remote user notification. \(userInfo)")
        
        
        /// parse recieving data to DataModel
        pushNotificationData = PushNotificationDataModelClass(fromDictionary: userInfo as! [String:Any])
        
        
        /// set badgeNumber
        UserNotificationManager.shared.setBadgeNumber(to: (pushNotificationData?.aps.badge)!)
        
        
        /// show notification banner when the app is in foreground.
        /// See NotificationBanner/ for more Detail.
        let notificationBanner = NotificationBanner.loadFromNib()
        
        if pushNotificationData?.aps.contentAvailable == 1 {
            if let message = pushNotificationData?.aps.alert {
                notificationBanner.show(message: message.body)
            } else {
                notificationBanner.show()
            }
        } else {
            notificationBanner.show(isSilence: true)
        }
    }
    
    
    fileprivate func didRegisterRemoteNotification(token deviceToken: Data) {
        printUserNotificationLog("Device Token: \(deviceToken.convertTokenDataToString)")
        delegate?.getDeviceToken(token: deviceToken.convertTokenDataToString)
    }
    
    
    fileprivate func didFailRegisterRemoteNotification(error: Error) {
        printUserNotificationLog("Fail to register remote Notification with error: \(error.localizedDescription)")
    }
    
    
    private func setBadgeNumber(to count:Int) {
        UIApplication.shared.applicationIconBadgeNumber = count
        printUserNotificationLog("Set application icon badge number to \(count)")
    }
}


/// Router for UserNotificationLog
extension UserNotificationManager {
    fileprivate func printUserNotificationLog(_ message: String) {
        if isDebugEnable {
            UserNotificationLog(message)
        }
    }
}


fileprivate struct UserNotificationLog {
    @discardableResult
    init(_ message: String) {
        NSLog("[UserNotification] \(message)")
    }
}


/// MARK: - AppDelegate
///
extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        UserNotificationManager.shared.didRegisterRemoteNotification(token: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        UserNotificationManager.shared.didFailRegisterRemoteNotification(error: error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        UserNotificationManager.shared.didReceiveRemoteNotification(userInfo: userInfo)
    }
}

