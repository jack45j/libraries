//
//  NotificationBannerUtilities.swift
//  CurDr_TW_Patient
//
//  Created by 林翌埕 on 2018/10/29.
//  Copyright © 2018 Benson Lin. All rights reserved.
//

import UIKit

class NotificationBannerUtilities: NSObject {
    
    class func isNotchFeaturedIPhone() -> Bool {
        if #available(iOS 11, *) {
            if UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0 > CGFloat(0) {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
}
