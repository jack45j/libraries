//
//  NotificationBanner.swift
//  CurDr_TW_Patient
//
//  Created by 林翌埕 on 2018/10/29.
//  Copyright © 2018 Benson Lin. All rights reserved.
//

import UIKit
import AudioToolbox
import Reusable

class NotificationBanner: UIView, NibLoadable {
    
//    static let shared = NotificationBanner()
    
    @IBOutlet weak var bannerMessageLabel: UILabel!
    
    private var bannerHeight: CGFloat = 64.0
    
    private var isPresenting: Bool = false
    
    /// The main window of the application which banner views are placed on
    private let appWindow: UIWindow = UIApplication.shared.delegate!.window!!
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        if shouldAdjustForNotchFeaturedIphone() { bannerHeight = 88 }
        self.frame = CGRect(x: 0, y: -bannerHeight, width: appWindow.frame.width, height: bannerHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    internal func show(message: String? = "您有新訊息", isSilence:Bool = false) {
        self.isPresenting = true
        if !isSilence {
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            bannerMessageLabel.text = message!
            appWindow.addSubview(self)
            appWindow.windowLevel = UIWindow.Level.statusBar + 1
            
            self.frame = CGRect(x: 0, y: -bannerHeight, width: appWindow.frame.width, height: bannerHeight)
            UIView.animate(withDuration: 0.3,
                           delay: 0.0,
                           usingSpringWithDamping: 1,
                           initialSpringVelocity: 1,
                           options: [.curveLinear, .allowUserInteraction],
                           animations: {
                            self.frame = CGRect(x: 0, y: 0, width: self.appWindow.frame.width, height: self.bannerHeight)
            }) { (completed) in
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                    self.dismiss()
                })
            }
        }
    }
    
    
    internal func dismiss() {
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 1,
                       options: [.curveLinear, .allowUserInteraction],
                       animations: {
                        self.frame = CGRect(x: 0, y: -self.bannerHeight, width: self.appWindow.frame.width, height: self.bannerHeight)
        }) { (completed) in
            if self.isPresenting {
                self.appWindow.windowLevel = UIWindow.Level.normal
                self.removeFromSuperview()
            }
            self.isPresenting = false
        }
    }
    
    
    @IBAction func pressedDismissButton(_ sender: Any) {
        if isPresenting { dismiss() }
    }
}


extension NotificationBanner {
    private func shouldAdjustForNotchFeaturedIphone() -> Bool {
        return NotificationBannerUtilities.isNotchFeaturedIPhone()
    }
}
