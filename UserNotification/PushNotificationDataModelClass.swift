//
//  PushNotificationDataModelClass.swift
//  CurDr_TW_Patient
//
//  Created by 林翌埕 on 2018/10/25.
//  Copyright © 2018 Benson Lin. All rights reserved.
//

import Foundation

class PushNotificationDataModelClass : NSObject, NSCoding{
    
    var aps : Aps!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        if let apsData = dictionary["aps"] as? [String:Any]{
            aps = Aps(fromDictionary: apsData)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if aps != nil{
            dictionary["aps"] = aps.toDictionary()
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        aps = aDecoder.decodeObject(forKey: "aps") as? Aps
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if aps != nil{
            aCoder.encode(aps, forKey: "aps")
        }
    }
}


class Aps : NSObject, NSCoding{
    
    var alert : Alert!
    var badge : Int!
    var contentAvailable : Int!
    var sound : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        badge = dictionary["badge"] as? Int
        contentAvailable = dictionary["content-available"] as? Int
        sound = dictionary["sound"] as? String
        if let alertData = dictionary["alert"] as? [String:Any]{
            alert = Alert(fromDictionary: alertData)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if badge != nil{
            dictionary["badge"] = badge
        }
        if contentAvailable != nil{
            dictionary["content-available"] = contentAvailable
        }
        if sound != nil{
            dictionary["sound"] = sound
        }
        if alert != nil{
            dictionary["alert"] = alert!.toDictionary()
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        alert = aDecoder.decodeObject(forKey: "alert") as? Alert
        badge = aDecoder.decodeObject(forKey: "badge") as? Int
        contentAvailable = aDecoder.decodeObject(forKey: "content-available") as? Int
        sound = aDecoder.decodeObject(forKey: "sound") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if alert != nil{
            aCoder.encode(alert, forKey: "alert")
        }
        if badge != nil{
            aCoder.encode(badge, forKey: "badge")
        }
        if contentAvailable != nil{
            aCoder.encode(contentAvailable, forKey: "content-available")
        }
        if sound != nil{
            aCoder.encode(sound, forKey: "sound")
        }
    }
}


class Alert : NSObject, NSCoding{
    
    var body : String?
    var title : String?
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        body = dictionary["body"] as? String
        title = dictionary["title"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if body != nil{
            dictionary["body"] = body
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        body = aDecoder.decodeObject(forKey: "body") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if body != nil{
            aCoder.encode(body, forKey: "body")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
    }
}
