//
//  Bool+Additions.swift
//  CurDr_TW_Patient
//
//  Created by 林翌埕 on 2019/1/23.
//  Copyright © 2019 Benson Lin. All rights reserved.
//

import Foundation

extension Bool {
    var intValue: Int {
        return self ? 1 : 0
    }
}

extension Int {
    var boolValue: Bool {
        return self != 0
    }
}
